package com.blueberry.listitems.users.mapper

import com.blueberry.listitems.utils.DataUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class UserEntityToModelMapperTest {

    private val mapper = UserEntityToModelMapper()

    @Test
    internal fun `should map userEntity to userModel`() {
        val expected = DataUtils.getUserModel()

        val actual = mapper.map(DataUtils.getUserEntity())

        assertEquals(actual, expected)
    }
}