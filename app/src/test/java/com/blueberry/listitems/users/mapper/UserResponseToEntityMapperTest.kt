package com.blueberry.listitems.users.mapper

import com.blueberry.listitems.utils.DataUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class UserResponseToEntityMapperTest {

    private val mapper = UserResponseToEntityMapper()

    @Test
    internal fun `should map userResponse to userEntity`() {
        val expected = DataUtils.getUserEntity()

        val actual = mapper.map(DataUtils.getResponseUser())

        assertEquals(actual, expected)
    }
}