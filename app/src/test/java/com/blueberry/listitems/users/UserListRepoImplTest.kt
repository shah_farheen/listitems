package com.blueberry.listitems.users

import com.blueberry.listitems.db.dao.UserDao
import com.blueberry.listitems.helper.TestDispatchersProvider
import com.blueberry.listitems.userService.UserService
import com.blueberry.listitems.userService.model.UserListResponse
import com.blueberry.listitems.utils.DataUtils
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

@ExperimentalCoroutinesApi
internal class UserListRepoImplTest {

    private val userDao: UserDao = mock()
    private val userService: UserService = mock()

    private val dispatchersProvider = TestDispatchersProvider()

    private val repo = UserListRepoImpl(
        userDao = userDao,
        userService = userService,
        dispatchersProvider = dispatchersProvider
    )

    @Test
    internal fun `should return local stored list of users`() {
        runBlocking {
            val expected = DataUtils.userEntityList

            whenever(userDao.getAllUsers())
                .thenReturn(expected)

            val actual = repo.getUserListFromLocal()

            assertEquals(actual, expected)
        }
    }

    @Test
    internal fun `should update users in local db`() {
        runBlocking {
            // when called
            repo.updateUsersInLocal(DataUtils.userEntityList)

            // then verify
            verify(userDao).updateUsers(DataUtils.userEntityList)
        }
    }

    @Test
    internal fun `should return user list from remote on successful response`() {
        runBlocking {
            val serviceCall: Call<UserListResponse> = mock()
            val serviceResponse: Response<UserListResponse> = mock()

            val expected = UserListResponse(DataUtils.userResponseList)

            whenever(userService.getUsersList())
                .thenReturn(serviceCall)
            whenever(serviceCall.execute())
                .thenReturn(serviceResponse)
            whenever(serviceResponse.isSuccessful)
                .thenReturn(true)
            whenever(serviceResponse.body())
                .thenReturn(expected)

            val actual = repo.getUserListFromRemote()

            assertEquals(actual, expected)
        }
    }

    @Test
    internal fun `should return null when response is not successful`() {
        runBlocking {
            val serviceCall: Call<UserListResponse> = mock()
            val serviceResponse: Response<UserListResponse> = mock()

            whenever(userService.getUsersList())
                .thenReturn(serviceCall)
            whenever(serviceCall.execute())
                .thenReturn(serviceResponse)
            whenever(serviceResponse.isSuccessful)
                .thenReturn(false)

            assertNull(repo.getUserListFromRemote())
        }
    }

    @Test
    internal fun `should return null if exception thrown by service`() {
        runBlocking {
            val serviceCall: Call<UserListResponse> = mock()

            whenever(userService.getUsersList())
                .thenReturn(serviceCall)
            whenever(serviceCall.execute())
                .thenThrow(IOException("some-io-error"))

            assertNull(repo.getUserListFromRemote())
        }
    }
}