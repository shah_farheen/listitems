package com.blueberry.listitems.users

import com.blueberry.listitems.users.view.UserListView
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineScope
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
internal class UserListPresenterTest {

    private val userListView: UserListView = mock()
    private val userListUseCase: UserListUseCase = mock()

    private val presenter = UserListPresenter(userListView, userListUseCase)

    @BeforeEach
    fun setup() {
        whenever(userListView.getCoroutineScope())
            .thenReturn(TestCoroutineScope())

        presenter.viewCreated()
    }

    @Test
    internal fun `should set users on view when use-case returns a non null user list`() {
        runBlocking {
            whenever(userListUseCase.getUserList())
                .thenReturn(listOf())

            presenter.getUserList()

            verify(userListView).getCoroutineScope()
            verify(userListView).setUserList(listOf())
        }
    }

    @Test
    internal fun `should not set users to view when use-case returns a null list`() {
        runBlocking {
            whenever(userListUseCase.getUserList())
                .thenReturn(null)

            presenter.getUserList()

            verify(userListView).getCoroutineScope()
        }
    }

    @AfterEach
    fun tearDown() {
        verifyNoMoreInteractions(userListView)
    }
}