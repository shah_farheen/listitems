package com.blueberry.listitems.users.mapper

import com.blueberry.listitems.utils.DataUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class UserResponseToModelMapperTest {

    private val mapper = UserResponseToModelMapper()

    @Test
    internal fun `should map userResponse to userModel`() {
        val expected = DataUtils.getUserModel()

        val actual = mapper.map(DataUtils.getResponseUser())

        assertEquals(actual, expected)
    }
}