package com.blueberry.listitems.users

import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.mapper.Mapper
import com.blueberry.listitems.userService.model.User
import com.blueberry.listitems.userService.model.UserListResponse
import com.blueberry.listitems.users.model.UserModel
import com.blueberry.listitems.utils.DataUtils
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class UserListInteractorTest {

    private val userListRepo: UserListRepo = mock()
    private val userResponseToModelMapper: Mapper<User, UserModel> = mock()
    private val userResponseToEntityMapper: Mapper<User, UserEntity> = mock()
    private val userEntityToModelMapper: Mapper<UserEntity, UserModel> = mock()

    private val useCase = UserListInteractor(
        userListRepo = userListRepo,
        userResponseToModelMapper = userResponseToModelMapper,
        userResponseToEntityMapper = userResponseToEntityMapper,
        userEntityToModelMapper = userEntityToModelMapper
    )

    @BeforeEach
    fun setup() {
        whenever(userResponseToModelMapper.map(DataUtils.getResponseUser()))
            .thenReturn(DataUtils.getUserModel())
        whenever(userResponseToEntityMapper.map(DataUtils.getResponseUser()))
            .thenReturn(DataUtils.getUserEntity())
        whenever(userEntityToModelMapper.map(DataUtils.getUserEntity()))
            .thenReturn(DataUtils.getUserModel())
    }

    @Test
    internal fun `should return user list and save to local db if response received from remote`() {
        runBlocking {
            whenever(userListRepo.getUserListFromRemote())
                .thenReturn(UserListResponse(listOf(DataUtils.getResponseUser())))

            val actual = useCase.getUserList()
            val expected = listOf(DataUtils.getUserModel())

            assertEquals(actual, expected)

            verify(userListRepo).updateUsersInLocal(listOf(DataUtils.getUserEntity()))
        }
    }

    @Test
    internal fun `should return list from local if no response from remote`() {
        runBlocking {
            whenever(userListRepo.getUserListFromRemote())
                .thenReturn(null)
            whenever(userListRepo.getUserListFromLocal())
                .thenReturn(listOf(DataUtils.getUserEntity()))

            val actual = useCase.getUserList()
            val expected = listOf(DataUtils.getUserModel())

            assertEquals(actual, expected)
        }
    }

    @Test
    internal fun `should return null for user list if null returned from remote and local both`() {
        runBlocking {
            whenever(userListRepo.getUserListFromRemote())
                .thenReturn(null)
            whenever(userListRepo.getUserListFromLocal())
                .thenReturn(null)

            assertNull(useCase.getUserList())
        }
    }
}