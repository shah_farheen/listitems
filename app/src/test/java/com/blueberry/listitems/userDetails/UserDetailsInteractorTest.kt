package com.blueberry.listitems.userDetails

import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.mapper.Mapper
import com.blueberry.listitems.userService.model.User
import com.blueberry.listitems.userService.model.UserDetailResponse
import com.blueberry.listitems.users.model.UserModel
import com.blueberry.listitems.utils.DataUtils
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class UserDetailsInteractorTest {

    private val userId = 1
    private val userDetailsRepo: UserDetailsRepo = mock()
    private val userResponseToModelMapper: Mapper<User, UserModel> = mock()
    private val userResponseToEntityMapper: Mapper<User, UserEntity> = mock()
    private val userEntityToModelMapper: Mapper<UserEntity, UserModel> = mock()

    private val useCase = UserDetailsInteractor(
        userDetailsRepo = userDetailsRepo,
        userResponseToModelMapper = userResponseToModelMapper,
        userResponseToEntityMapper = userResponseToEntityMapper,
        userEntityToModelMapper = userEntityToModelMapper
    )

    @BeforeEach
    fun setup() {
        whenever(userResponseToModelMapper.map(DataUtils.getResponseUser()))
            .thenReturn(DataUtils.getUserModel())
        whenever(userResponseToEntityMapper.map(DataUtils.getResponseUser()))
            .thenReturn(DataUtils.getUserEntity())
        whenever(userEntityToModelMapper.map(DataUtils.getUserEntity()))
            .thenReturn(DataUtils.getUserModel())
    }

    @Test
    internal fun `should return user and save to local db if response received from remote`() {
        runBlocking {
            whenever(userDetailsRepo.getUserDetailsFromRemote(userId))
                .thenReturn(UserDetailResponse(DataUtils.getResponseUser()))

            val actual = useCase.getUserDetails(userId)
            val expected = DataUtils.getUserModel()

            assertEquals(actual, expected)

            verify(userDetailsRepo).updateUserDetailsInLocal(DataUtils.getUserEntity())
        }
    }

    @Test
    internal fun `should return user from local if null response from remote`() {
        runBlocking {
            whenever(userDetailsRepo.getUserDetailsFromRemote(userId))
                .thenReturn(null)
            whenever(userDetailsRepo.getUserDetailsFromLocal(userId))
                .thenReturn(DataUtils.getUserEntity())

            val actual = useCase.getUserDetails(userId)
            val expected = DataUtils.getUserModel()

            assertEquals(actual, expected)
        }
    }

    @Test
    internal fun `should return null for user if null returned from remote and local both`() {
        runBlocking {
            whenever(userDetailsRepo.getUserDetailsFromRemote(userId))
                .thenReturn(null)
            whenever(userDetailsRepo.getUserDetailsFromLocal(userId))
                .thenReturn(null)

            assertNull(useCase.getUserDetails(userId))
        }
    }
}