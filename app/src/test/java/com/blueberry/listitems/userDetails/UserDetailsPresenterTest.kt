package com.blueberry.listitems.userDetails

import com.blueberry.listitems.userDetails.view.UserDetailsView
import com.blueberry.listitems.utils.DataUtils
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineScope
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
internal class UserDetailsPresenterTest {

    private val userId = 1
    private val userDetailsView: UserDetailsView = mock()
    private val userDetailsUseCase: UserDetailsUseCase = mock()

    private val presenter = UserDetailsPresenter(userDetailsView, userDetailsUseCase)

    @BeforeEach
    fun setup() {
        whenever(userDetailsView.getCoroutineScope())
            .thenReturn(TestCoroutineScope())

        presenter.viewCreated()
    }

    @Test
    internal fun `should set user details on view when use-case returns a non null user`() {
        runBlocking {
            whenever(userDetailsUseCase.getUserDetails(userId))
                .thenReturn(DataUtils.getUserModel())

            presenter.getUserDetails(userId)

            verify(userDetailsView).getCoroutineScope()
            verify(userDetailsView).setUserDetails(DataUtils.getUserModel())
        }
    }

    @Test
    internal fun `should not set user details to view when use-case returns a null user`() {
        runBlocking {
            whenever(userDetailsUseCase.getUserDetails(userId))
                .thenReturn(null)

            presenter.getUserDetails(userId)

            verify(userDetailsView).getCoroutineScope()
        }
    }

    @AfterEach
    fun tearDown() {
        verifyNoMoreInteractions(userDetailsView)
    }
}