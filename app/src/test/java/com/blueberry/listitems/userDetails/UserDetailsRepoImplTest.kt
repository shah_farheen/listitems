package com.blueberry.listitems.userDetails

import com.blueberry.listitems.db.dao.UserDao
import com.blueberry.listitems.helper.TestDispatchersProvider
import com.blueberry.listitems.userService.UserService
import com.blueberry.listitems.userService.model.UserDetailResponse
import com.blueberry.listitems.utils.DataUtils
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import retrofit2.Call
import retrofit2.Response
import java.io.IOException

@ExperimentalCoroutinesApi
internal class UserDetailsRepoImplTest {

    private val userDao: UserDao = mock()
    private val userService: UserService = mock()

    private val dispatchersProvider = TestDispatchersProvider()

    private val repo = UserDetailsRepoImpl(
        userDao = userDao,
        userService = userService,
        dispatchersProvider = dispatchersProvider
    )

    @Test
    internal fun `should return locally stored user`() {
        runBlocking {
            val userId = 1
            val expected = DataUtils.getUserEntity()

            whenever(userDao.getUser(userId))
                .thenReturn(expected)

            val actual = repo.getUserDetailsFromLocal(userId)

            assertEquals(actual, expected)
        }
    }

    @Test
    internal fun `should update user in local db`() {
        runBlocking {
            // when called
            repo.updateUserDetailsInLocal(DataUtils.getUserEntity())

            // then verify
            verify(userDao).insertUsers(listOf(DataUtils.getUserEntity()))
        }
    }

    @Test
    internal fun `should return user from remote on successful response`() {
        runBlocking {
            val userId = 1
            val serviceCall: Call<UserDetailResponse> = mock()
            val serviceResponse: Response<UserDetailResponse> = mock()

            val expected = UserDetailResponse(DataUtils.getResponseUser())

            whenever(userService.getUserDetails(userId))
                .thenReturn(serviceCall)
            whenever(serviceCall.execute())
                .thenReturn(serviceResponse)
            whenever(serviceResponse.isSuccessful)
                .thenReturn(true)
            whenever(serviceResponse.body())
                .thenReturn(expected)

            val actual = repo.getUserDetailsFromRemote(userId)

            assertEquals(actual, expected)
        }
    }

    @Test
    internal fun `should return null when response is not successful`() {
        runBlocking {
            val userId = 1
            val serviceCall: Call<UserDetailResponse> = mock()
            val serviceResponse: Response<UserDetailResponse> = mock()

            whenever(userService.getUserDetails(userId))
                .thenReturn(serviceCall)
            whenever(serviceCall.execute())
                .thenReturn(serviceResponse)
            whenever(serviceResponse.isSuccessful)
                .thenReturn(false)

            assertNull(repo.getUserDetailsFromRemote(userId))
        }
    }

    @Test
    internal fun `should return null if exception thrown by service`() {
        runBlocking {
            val userId = 1
            val serviceCall: Call<UserDetailResponse> = mock()

            whenever(userService.getUserDetails(userId))
                .thenReturn(serviceCall)
            whenever(serviceCall.execute())
                .thenThrow(IOException("some-io-error"))

            assertNull(repo.getUserDetailsFromRemote(userId))
        }
    }
}