package com.blueberry.listitems.helper

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher

@ExperimentalCoroutinesApi
internal class TestDispatchersProvider : DispatchersProvider {

    override fun io() = TestCoroutineDispatcher()

    override fun computation() = TestCoroutineDispatcher()

    override fun main() = TestCoroutineDispatcher()
}