package com.blueberry.listitems.utils

import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.userService.model.User
import com.blueberry.listitems.users.model.UserModel

object DataUtils {

    val userResponseList: List<User>
        get() {
            return listOf(
                User(
                    1,
                    "John",
                    "Doe",
                    "doejohn@email.com",
                    "avatar.doe.john"
                ),
                User(
                    2,
                    "Jane",
                    "Doe",
                    "doejane@email.com",
                    "avatar.doe.jane"
                )
            )
        }

    val userModelList: List<UserModel>
        get() {
            return listOf(
                UserModel(
                    1,
                    "John Doe",
                    "doejohn@email.com",
                    "avatar.doe.john"
                ),
                UserModel(
                    2,
                    "Jane Doe",
                    "doejane@email.com",
                    "avatar.doe.jane"
                )
            )
        }

    val userEntityList: List<UserEntity>
        get() {
            return listOf(
                UserEntity(
                    1,
                    "John",
                    "Doe",
                    "doejohn@email.com",
                    "avatar.doe.john"
                ),
                UserEntity(
                    2,
                    "Jane",
                    "Doe",
                    "doejane@email.com",
                    "avatar.doe.jane"
                )
            )
        }

    fun getResponseUser(): User = userResponseList[0]
    fun getUserModel(): UserModel = userModelList[0]
    fun getUserEntity(): UserEntity = userEntityList[0]
}