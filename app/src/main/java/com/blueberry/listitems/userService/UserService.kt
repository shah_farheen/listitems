package com.blueberry.listitems.userService

import com.blueberry.listitems.userService.model.UserDetailResponse
import com.blueberry.listitems.userService.model.UserListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface UserService {

    @GET("users")
    fun getUsersList(@Query("per_page") itemsPerPage: Int = 12): Call<UserListResponse>

    @GET("users/{user_id}")
    fun getUserDetails(@Path("user_id") userId: Int): Call<UserDetailResponse>
}