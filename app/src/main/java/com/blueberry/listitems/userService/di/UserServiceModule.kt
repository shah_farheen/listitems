package com.blueberry.listitems.userService.di

import com.blueberry.listitems.network.NetworkSingleton
import com.blueberry.listitems.userService.UserService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UserServiceModule {

    @Provides
    @Singleton
    fun providesUserService(
        networkSingleton: NetworkSingleton
    ): UserService {
        return networkSingleton.getService(UserService::class.java)
    }
}