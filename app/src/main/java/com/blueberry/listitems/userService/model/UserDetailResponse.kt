package com.blueberry.listitems.userService.model

import com.google.gson.annotations.SerializedName

data class UserDetailResponse(
    @field:SerializedName("data") val user: User
)
