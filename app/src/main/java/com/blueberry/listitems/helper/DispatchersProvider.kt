package com.blueberry.listitems.helper

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

interface DispatchersProvider {
    fun io(): CoroutineDispatcher
    fun computation(): CoroutineDispatcher
    fun main(): CoroutineDispatcher
}

class DispatchersProviderImpl : DispatchersProvider {

    override fun io() = Dispatchers.IO

    override fun computation() = Dispatchers.Default

    override fun main() = Dispatchers.Main
}