package com.blueberry.listitems.di

import com.blueberry.listitems.ListItemApplication
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ApplicationModule::class,
        ScreenInjectorModule::class
    ]
)
interface ApplicationComponent {

    fun inject(application: ListItemApplication)
}