package com.blueberry.listitems.di

import android.content.Context
import com.blueberry.listitems.BuildConfig
import com.blueberry.listitems.ListItemApplication
import com.blueberry.listitems.db.di.DatabaseModule
import com.blueberry.listitems.helper.DispatchersProvider
import com.blueberry.listitems.helper.DispatchersProviderImpl
import com.blueberry.listitems.network.NetworkSingleton
import com.blueberry.listitems.userService.di.UserServiceModule
import com.blueberry.listitems.utils.Utils
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module(
    includes = [
        UserServiceModule::class,
        DatabaseModule::class
    ]
)
class ApplicationModule(private val application: ListItemApplication) {

    @Provides
    @Singleton
    fun providesNetworkSingleton(
        @Named(Utils.NAMED_BASE_URL) baseUrl: String
    ): NetworkSingleton {
        return NetworkSingleton(baseUrl)
    }

    @Provides
    @Named(Utils.NAMED_BASE_URL)
    fun providesBaseUrl(): String {
        return BuildConfig.BASE_URL
    }

    @Provides
    @ApplicationContext
    fun providesApplicationContext(): Context {
        return application.applicationContext
    }

    @Provides
    fun providesDispatchersProvider(): DispatchersProvider = DispatchersProviderImpl()
}