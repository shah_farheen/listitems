package com.blueberry.listitems.di

import com.blueberry.listitems.userDetails.di.UserDetailsScreenModule
import com.blueberry.listitems.users.di.UserListScreenModule
import dagger.Module

@Module(
    includes = [
        UserListScreenModule::class,
        UserDetailsScreenModule::class
    ]
)
class ScreenInjectorModule