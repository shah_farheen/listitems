package com.blueberry.listitems.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide

object Utils {

    const val NAMED_BASE_URL = "NAMED_BASE_URL"

    fun loadImageFromUrl(context: Context, imageUrl: String, target: ImageView) {
        Glide.with(context)
            .load(imageUrl)
            .into(target)
    }
}