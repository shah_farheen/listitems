package com.blueberry.listitems.db.di

import android.content.Context
import androidx.room.Room
import com.blueberry.listitems.db.AppDatabase
import com.blueberry.listitems.db.Constants.APP_DB_NAME
import com.blueberry.listitems.db.dao.UserDao
import com.blueberry.listitems.di.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    fun providesUserDao(
        appDatabase: AppDatabase
    ): UserDao {
        return appDatabase.userDao()
    }

    @Provides
    @Singleton
    fun providesAppDatabase(
        @ApplicationContext context: Context
    ): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, APP_DB_NAME)
            .build()
    }
}