package com.blueberry.listitems.db.dao

import androidx.room.*
import com.blueberry.listitems.db.Constants.TABLE_USER
import com.blueberry.listitems.db.entity.UserEntity

@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUsers(users: List<UserEntity>)

    @Query("DELETE FROM $TABLE_USER")
    fun deleteAllUsers()

    @Query("SELECT * FROM $TABLE_USER")
    fun getAllUsers(): List<UserEntity>?

    @Query("SELECT * FROM $TABLE_USER WHERE id = :id")
    fun getUser(id: Int): UserEntity?

    @Transaction
    fun updateUsers(users: List<UserEntity>) {
        deleteAllUsers()
        insertUsers(users)
    }
}