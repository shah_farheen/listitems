package com.blueberry.listitems.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.blueberry.listitems.db.Constants.TABLE_USER

@Entity(tableName = TABLE_USER)
data class UserEntity(
    @PrimaryKey
    val id: Int,
    val firstName: String,
    val lastName: String,
    val email: String,
    val pictureUrl: String
)