package com.blueberry.listitems.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.blueberry.listitems.db.Constants.APP_DB_VERSION
import com.blueberry.listitems.db.dao.UserDao
import com.blueberry.listitems.db.entity.UserEntity

@Database(
    entities = [
        UserEntity::class
    ],
    version = APP_DB_VERSION
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao
}