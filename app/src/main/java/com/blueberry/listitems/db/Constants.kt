package com.blueberry.listitems.db

object Constants {

    const val APP_DB_VERSION = 1

    const val APP_DB_NAME = "db_users"

    const val TABLE_USER = "users"
}