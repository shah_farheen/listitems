package com.blueberry.listitems.userDetails.view

import com.blueberry.listitems.base.BaseView
import com.blueberry.listitems.users.model.UserModel

interface UserDetailsView : BaseView {

    fun setUserDetails(userModel: UserModel)
}