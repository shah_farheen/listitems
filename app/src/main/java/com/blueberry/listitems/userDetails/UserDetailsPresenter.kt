package com.blueberry.listitems.userDetails

import com.blueberry.listitems.base.BasePresenter
import com.blueberry.listitems.userDetails.view.UserDetailsView
import kotlinx.coroutines.launch

class UserDetailsPresenter(
    view: UserDetailsView,
    private val userDetailsUseCase: UserDetailsUseCase
) : BasePresenter<UserDetailsView>(view) {

    fun getUserDetails(userId: Int) {
        presenterScope?.launch {
            userDetailsUseCase.getUserDetails(userId)?.let {
                view.setUserDetails(it)
            }
        }
    }
}