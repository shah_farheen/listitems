package com.blueberry.listitems.userDetails

import com.blueberry.listitems.db.dao.UserDao
import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.helper.DispatchersProvider
import com.blueberry.listitems.userService.UserService
import com.blueberry.listitems.userService.model.UserDetailResponse
import kotlinx.coroutines.withContext
import java.io.IOException

interface UserDetailsRepo {
    suspend fun getUserDetailsFromLocal(userId: Int): UserEntity?

    suspend fun updateUserDetailsInLocal(user: UserEntity)

    suspend fun getUserDetailsFromRemote(userId: Int): UserDetailResponse?
}

@Suppress("BlockingMethodInNonBlockingContext")
class UserDetailsRepoImpl(
    private val userDao: UserDao,
    private val userService: UserService,
    private val dispatchersProvider: DispatchersProvider
) : UserDetailsRepo {

    override suspend fun getUserDetailsFromLocal(userId: Int): UserEntity? {
        return withContext(dispatchersProvider.io()) {
            userDao.getUser(userId)
        }
    }

    override suspend fun updateUserDetailsInLocal(user: UserEntity) {
        withContext(dispatchersProvider.io()) {
            userDao.insertUsers(listOf(user))
        }
    }

    override suspend fun getUserDetailsFromRemote(userId: Int): UserDetailResponse? {
        return withContext(dispatchersProvider.io()) {
            try {
                val response = userService.getUserDetails(userId).execute()
                if (response.isSuccessful) {
                    response.body()
                } else {
                    null
                }
            } catch (ioException: IOException) {
                null
            }
        }
    }
}