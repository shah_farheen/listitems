package com.blueberry.listitems.userDetails.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blueberry.listitems.base.BaseFragment
import com.blueberry.listitems.databinding.FragmentUserDetailsBinding
import com.blueberry.listitems.userDetails.UserDetailsPresenter
import com.blueberry.listitems.users.model.UserModel
import com.blueberry.listitems.utils.Utils

class UserDetailsFragment : BaseFragment<UserDetailsPresenter>(), UserDetailsView {

    private var binding: FragmentUserDetailsBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentUserDetailsBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val userId = arguments?.getInt(PARAM_USER_ID, -1)
        userId?.let {
            if (it != -1) {
                presenter.getUserDetails(it)
            }
        }
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun setUserDetails(userModel: UserModel) {
        binding?.let {
            Utils.loadImageFromUrl(requireContext(), userModel.pictureUrl, it.imageUserDp)
            it.textUserName.text = userModel.name
            it.textUserEmail.text = userModel.email
        }
    }

    companion object {

        const val TAG_USER_DETAILS = "TAG_USER_DETAILS"
        private const val PARAM_USER_ID = "PARAM_USER_ID"

        fun getInstance(userId: Int): UserDetailsFragment {
            return UserDetailsFragment()
                .also {
                    it.arguments = Bundle()
                        .apply {
                            putInt(PARAM_USER_ID, userId)
                        }
                }
        }
    }
}