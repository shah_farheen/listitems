package com.blueberry.listitems.userDetails.di

import com.blueberry.listitems.userDetails.view.UserDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UserDetailsScreenModule {

    @ContributesAndroidInjector(modules = [UserDetailsModule::class])
    abstract fun contributesUserDetailFragment(): UserDetailsFragment
}