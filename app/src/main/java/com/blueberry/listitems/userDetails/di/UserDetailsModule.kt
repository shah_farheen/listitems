package com.blueberry.listitems.userDetails.di

import com.blueberry.listitems.db.dao.UserDao
import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.helper.DispatchersProvider
import com.blueberry.listitems.mapper.Mapper
import com.blueberry.listitems.userDetails.*
import com.blueberry.listitems.userDetails.view.UserDetailsFragment
import com.blueberry.listitems.userService.UserService
import com.blueberry.listitems.userService.model.User
import com.blueberry.listitems.users.di.UserMapperModule
import com.blueberry.listitems.users.model.UserModel
import dagger.Module
import dagger.Provides

@Module(
    includes = [
        UserMapperModule::class
    ]
)
class UserDetailsModule {

    @Provides
    fun providesUserDetailsPresenter(
        fragment: UserDetailsFragment,
        userDetailsUseCase: UserDetailsUseCase
    ): UserDetailsPresenter {
        return UserDetailsPresenter(fragment, userDetailsUseCase)
    }

    @Provides
    fun providesUserDetailsUseCase(
        userDetailsRepo: UserDetailsRepo,
        userResponseToModelMapper: Mapper<User, UserModel>,
        userResponseToEntityMapper: Mapper<User, UserEntity>,
        userEntityToModelMapper: Mapper<UserEntity, UserModel>
    ): UserDetailsUseCase {
        return UserDetailsInteractor(
            userDetailsRepo = userDetailsRepo,
            userResponseToModelMapper = userResponseToModelMapper,
            userResponseToEntityMapper = userResponseToEntityMapper,
            userEntityToModelMapper = userEntityToModelMapper
        )
    }

    @Provides
    fun providesUserDetailsRepo(
        userDao: UserDao,
        userService: UserService,
        dispatchersProvider: DispatchersProvider
    ): UserDetailsRepo {
        return UserDetailsRepoImpl(
            userDao = userDao,
            userService = userService,
            dispatchersProvider = dispatchersProvider
        )
    }
}