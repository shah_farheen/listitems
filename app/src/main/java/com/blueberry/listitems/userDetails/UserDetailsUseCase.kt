package com.blueberry.listitems.userDetails

import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.mapper.Mapper
import com.blueberry.listitems.userService.model.User
import com.blueberry.listitems.users.model.UserModel

interface UserDetailsUseCase {

    suspend fun getUserDetails(userId: Int): UserModel?
}

class UserDetailsInteractor(
    private val userDetailsRepo: UserDetailsRepo,
    private val userResponseToModelMapper: Mapper<User, UserModel>,
    private val userResponseToEntityMapper: Mapper<User, UserEntity>,
    private val userEntityToModelMapper: Mapper<UserEntity, UserModel>
) : UserDetailsUseCase {

    override suspend fun getUserDetails(userId: Int): UserModel? {
        return userDetailsRepo.getUserDetailsFromRemote(userId)?.let { response ->

            userDetailsRepo.updateUserDetailsInLocal(
                userResponseToEntityMapper.map(response.user)
            )

            userResponseToModelMapper.map(response.user)
        } ?: kotlin.run {
            userDetailsRepo.getUserDetailsFromLocal(userId)?.let {
                userEntityToModelMapper.map(it)
            }
        }
    }
}