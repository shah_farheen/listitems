package com.blueberry.listitems.mapper

interface Mapper<INPUT, OUTPUT> {

    fun map(input: INPUT): OUTPUT
}