package com.blueberry.listitems.base

import kotlinx.coroutines.CoroutineScope

abstract class BasePresenter<out V : BaseView>(val view: V) {

    protected var presenterScope: CoroutineScope? = null

    open fun viewCreated() {
        presenterScope = view.getCoroutineScope()
    }

    open fun viewDestroyed() {
        presenterScope = null
    }
}