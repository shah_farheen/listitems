package com.blueberry.listitems.base

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import dagger.android.support.AndroidSupportInjection
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

abstract class BaseFragment<P : BasePresenter<BaseView>> : Fragment(), BaseView {

    @Inject
    protected lateinit var presenter: P

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.viewCreated()
    }

    override fun onDestroyView() {
        presenter.viewDestroyed()
        super.onDestroyView()
    }

    override fun getCoroutineScope(): CoroutineScope = viewLifecycleOwner.lifecycleScope
}