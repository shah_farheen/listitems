package com.blueberry.listitems.base

import kotlinx.coroutines.CoroutineScope

interface BaseView {

    fun getCoroutineScope(): CoroutineScope
}