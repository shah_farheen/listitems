package com.blueberry.listitems.users.mapper

import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.mapper.Mapper
import com.blueberry.listitems.users.model.UserModel

class UserEntityToModelMapper : Mapper<UserEntity, UserModel> {

    override fun map(input: UserEntity): UserModel {
        return UserModel(
            id = input.id,
            name = "${input.firstName} ${input.lastName}",
            email = input.email,
            pictureUrl = input.pictureUrl
        )
    }
}