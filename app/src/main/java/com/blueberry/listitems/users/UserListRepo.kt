package com.blueberry.listitems.users

import com.blueberry.listitems.db.dao.UserDao
import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.helper.DispatchersProvider
import com.blueberry.listitems.userService.UserService
import com.blueberry.listitems.userService.model.UserListResponse
import kotlinx.coroutines.withContext
import java.io.IOException

interface UserListRepo {

    suspend fun getUserListFromLocal(): List<UserEntity>?

    suspend fun updateUsersInLocal(users: List<UserEntity>)

    suspend fun getUserListFromRemote(): UserListResponse?
}

@Suppress("BlockingMethodInNonBlockingContext")
class UserListRepoImpl(
    private val userDao: UserDao,
    private val userService: UserService,
    private val dispatchersProvider: DispatchersProvider
) : UserListRepo {

    override suspend fun getUserListFromLocal(): List<UserEntity>? {
        return withContext(dispatchersProvider.io()) {
            userDao.getAllUsers()
        }
    }

    override suspend fun updateUsersInLocal(users: List<UserEntity>) {
        withContext(dispatchersProvider.io()) {
            userDao.updateUsers(users)
        }
    }

    override suspend fun getUserListFromRemote(): UserListResponse? {
        return withContext(dispatchersProvider.io()) {
            try {
                val response = userService.getUsersList().execute()
                if (response.isSuccessful) {
                    response.body()
                } else {
                    null
                }
            } catch (ioException: IOException) {
                null
            } catch (e: Exception) {
                null
            }
        }
    }
}