package com.blueberry.listitems.users.mapper

import com.blueberry.listitems.mapper.Mapper
import com.blueberry.listitems.userService.model.User
import com.blueberry.listitems.users.model.UserModel

class UserResponseToModelMapper : Mapper<User, UserModel> {

    override fun map(input: User): UserModel {
        return UserModel(
            id = input.id,
            name = "${input.firstName} ${input.lastName}",
            email = input.email,
            pictureUrl = input.avatar
        )
    }
}