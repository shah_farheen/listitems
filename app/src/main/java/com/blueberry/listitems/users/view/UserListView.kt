package com.blueberry.listitems.users.view

import com.blueberry.listitems.base.BaseView
import com.blueberry.listitems.users.model.UserModel

interface UserListView : BaseView {
    fun setUserList(userList: List<UserModel>)
}