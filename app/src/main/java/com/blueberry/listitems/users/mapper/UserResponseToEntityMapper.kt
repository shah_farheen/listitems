package com.blueberry.listitems.users.mapper

import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.mapper.Mapper
import com.blueberry.listitems.userService.model.User

class UserResponseToEntityMapper : Mapper<User, UserEntity> {

    override fun map(input: User): UserEntity {
        return UserEntity(
            id = input.id,
            firstName = input.firstName,
            lastName = input.lastName,
            email = input.email,
            pictureUrl = input.avatar
        )
    }
}