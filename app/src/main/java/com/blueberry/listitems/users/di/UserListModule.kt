package com.blueberry.listitems.users.di

import com.blueberry.listitems.db.dao.UserDao
import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.helper.DispatchersProvider
import com.blueberry.listitems.mapper.Mapper
import com.blueberry.listitems.userService.UserService
import com.blueberry.listitems.userService.model.User
import com.blueberry.listitems.users.*
import com.blueberry.listitems.users.model.UserModel
import com.blueberry.listitems.users.view.UserListFragment
import dagger.Module
import dagger.Provides

@Module(
    includes = [
        UserMapperModule::class
    ]
)
class UserListModule {

    @Provides
    fun providesUserListPresenter(
        userListFragment: UserListFragment,
        userListUseCase: UserListUseCase
    ): UserListPresenter {
        return UserListPresenter(userListFragment, userListUseCase)
    }

    @Provides
    fun providesUserListUseCase(
        userListRepo: UserListRepo,
        userResponseToModelMapper: Mapper<User, UserModel>,
        userResponseToEntityMapper: Mapper<User, UserEntity>,
        userEntityToModelMapper: Mapper<UserEntity, UserModel>
    ): UserListUseCase {
        return UserListInteractor(
            userListRepo = userListRepo,
            userResponseToModelMapper = userResponseToModelMapper,
            userResponseToEntityMapper = userResponseToEntityMapper,
            userEntityToModelMapper = userEntityToModelMapper
        )
    }

    @Provides
    fun providesUserListRepo(
        userDao: UserDao,
        userService: UserService,
        dispatchersProvider: DispatchersProvider
    ): UserListRepo {
        return UserListRepoImpl(
            userDao = userDao,
            userService = userService,
            dispatchersProvider = dispatchersProvider
        )
    }
}