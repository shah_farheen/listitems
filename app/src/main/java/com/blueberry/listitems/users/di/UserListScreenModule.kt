package com.blueberry.listitems.users.di

import com.blueberry.listitems.users.view.UserListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UserListScreenModule {

    @ContributesAndroidInjector(modules = [UserListModule::class])
    abstract fun contributesUserListFragment(): UserListFragment
}