package com.blueberry.listitems.users.model

data class UserModel(
    val id: Int,
    val name: String,
    val email: String,
    val pictureUrl: String
)
