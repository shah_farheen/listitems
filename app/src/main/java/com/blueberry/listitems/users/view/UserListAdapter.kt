package com.blueberry.listitems.users.view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.blueberry.listitems.databinding.ItemUserBinding
import com.blueberry.listitems.users.model.UserModel
import com.blueberry.listitems.utils.Utils

class UserListAdapter(
    private val context: Context,
    private val usersList: MutableList<UserModel>,
    private val itemClickAction: (Int) -> Unit
) : RecyclerView.Adapter<UserListAdapter.UserViewHolder>() {

    fun updateUsers(users: List<UserModel>) {
        usersList.clear()
        usersList.addAll(users)
        notifyItemRangeInserted(0, users.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            ItemUserBinding.inflate(
                LayoutInflater.from(context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(usersList[position], context)
    }

    override fun getItemCount(): Int {
        return usersList.size
    }

    inner class UserViewHolder(
        private val binding: ItemUserBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    itemClickAction.invoke(usersList[adapterPosition].id)
                }
            }
        }

        fun bind(userModel: UserModel, context: Context) {
            binding.textUserName.text = userModel.name
            binding.textUserEmail.text = userModel.email
            Utils.loadImageFromUrl(context, userModel.pictureUrl, binding.imageUserDp)
        }
    }
}