package com.blueberry.listitems.users.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.blueberry.listitems.R
import com.blueberry.listitems.base.BaseFragment
import com.blueberry.listitems.databinding.FragmentUserListBinding
import com.blueberry.listitems.userDetails.view.UserDetailsFragment
import com.blueberry.listitems.users.UserListPresenter
import com.blueberry.listitems.users.model.UserModel

class UserListFragment : BaseFragment<UserListPresenter>(), UserListView {

    private var binding: FragmentUserListBinding? = null

    private val userListAdapter by lazy {
        UserListAdapter(requireContext(), arrayListOf()) {
            navigateToUserDetails(it)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentUserListBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.recyclerUsers?.adapter = userListAdapter
        presenter.getUserList()
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun setUserList(userList: List<UserModel>) {
        userListAdapter.updateUsers(userList)
    }

    private fun navigateToUserDetails(userId: Int) {
        parentFragmentManager.beginTransaction()
            .add(
                R.id.fragment_container_view,
                UserDetailsFragment.getInstance(userId),
                UserDetailsFragment.TAG_USER_DETAILS
            )
            .addToBackStack(UserDetailsFragment.TAG_USER_DETAILS)
            .commit()
    }
}