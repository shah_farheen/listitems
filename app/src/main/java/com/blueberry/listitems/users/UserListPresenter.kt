package com.blueberry.listitems.users

import com.blueberry.listitems.base.BasePresenter
import com.blueberry.listitems.users.view.UserListView
import kotlinx.coroutines.launch

class UserListPresenter(
    view: UserListView,
    private val userListUseCase: UserListUseCase
) : BasePresenter<UserListView>(view) {

    fun getUserList() {
        presenterScope?.launch {
            userListUseCase.getUserList()?.let {
                view.setUserList(it)
            }
        }
    }
}