package com.blueberry.listitems.users

import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.mapper.Mapper
import com.blueberry.listitems.userService.model.User
import com.blueberry.listitems.users.model.UserModel

interface UserListUseCase {

    suspend fun getUserList(): List<UserModel>?
}

class UserListInteractor(
    private val userListRepo: UserListRepo,
    private val userResponseToModelMapper: Mapper<User, UserModel>,
    private val userResponseToEntityMapper: Mapper<User, UserEntity>,
    private val userEntityToModelMapper: Mapper<UserEntity, UserModel>
) : UserListUseCase {

    override suspend fun getUserList(): List<UserModel>? {
        return userListRepo.getUserListFromRemote()?.let { response ->

            userListRepo.updateUsersInLocal(response.users.map { user ->
                userResponseToEntityMapper.map(user)
            })

            response.users.map { user ->
                userResponseToModelMapper.map(user)
            }
        } ?: kotlin.run {
            userListRepo.getUserListFromLocal()?.let { users ->
                users.map {
                    userEntityToModelMapper.map(it)
                }
            }
        }
    }
}