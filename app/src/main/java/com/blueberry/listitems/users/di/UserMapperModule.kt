package com.blueberry.listitems.users.di

import com.blueberry.listitems.db.entity.UserEntity
import com.blueberry.listitems.mapper.Mapper
import com.blueberry.listitems.userService.model.User
import com.blueberry.listitems.users.mapper.UserEntityToModelMapper
import com.blueberry.listitems.users.mapper.UserResponseToEntityMapper
import com.blueberry.listitems.users.mapper.UserResponseToModelMapper
import com.blueberry.listitems.users.model.UserModel
import dagger.Module
import dagger.Provides

@Module
class UserMapperModule {

    @Provides
    fun providesUserResponseToModelMapper(): Mapper<User, UserModel> {
        return UserResponseToModelMapper()
    }

    @Provides
    fun providesUserResponseToEntityMapper(): Mapper<User, UserEntity> {
        return UserResponseToEntityMapper()
    }

    @Provides
    fun providesUserEntityToModelMapper(): Mapper<UserEntity, UserModel> {
        return UserEntityToModelMapper()
    }
}